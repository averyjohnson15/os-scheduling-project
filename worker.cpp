//for information on program, see README or header comment on oss.cpp

//preprocessor directives
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <string.h>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <mqueue.h>
#include <ctime>
#include <sys/ipc.h>
#include <sys/msg.h>

using namespace std;

//macro and typedef to be used for sysV queue
#define PERMS 0644
typedef struct msgbuffer
{
	long mtype;
	int intData;
} msgbuffer;

//worker entry
int main(int argc, char* argv[])
{
	//declare resources for mqueue
	msgbuffer sndBuffer, rcvBuffer;
	int msqid = 0;
	key_t key;

	//ftok to get key for queue
	if ((key = ftok("msgq.txt", 1)) == -1)
	{
		printf("issue with ftok childside\n");
		exit(1);
	}

	//join msg queue that parent created
	if ((msqid = msgget(key, PERMS)) == -1)
	{
		printf("issue connecting to queue on childside\n");
		exit(1);
	}	

	srand(time(0));
	//loop through receiving and sending messages to/from OSS until completion
	while (true)
	{
		//receive msg from parent
		if (msgrcv(msqid, &rcvBuffer, sizeof(msgbuffer), getpid(), 0) == -1)
		{
			printf("issue receiving message on childside\n");
			exit(1);
		}
		int quantum = rcvBuffer.intData;
		//get a random number between 1 and 300. If 1-294, return quantum of 10000000, if 300 return a random negative number between -1 and -9999999
		//negative is returned if part of quantum was used and process is 'finished', between 295 to 299, return 1 to 9999999 to show blocked
		int returnValue = 0;
		int tempRand = rand() % 300 + 1;
		bool finishedFlag = false;

		if (tempRand <= 297)
			returnValue = quantum;
		else if (tempRand <= 299)
			returnValue = rand() % (quantum - 1) + 1;
		else
		{
			returnValue = (rand() % (quantum - 1) + 1) * -1;
			finishedFlag = true;
		}

		//send back relative value to OSS
		sndBuffer.mtype = getppid();
		sndBuffer.intData = returnValue;
		if (msgsnd(msqid, &sndBuffer, sizeof(msgbuffer) - sizeof(long), 0) == -1)
		{
			printf("issue sending message from childside\n");
			exit(1);
		}
		if (finishedFlag)
		{
			exit(0);
		}
	}		
        return 0;
}
