/***************************************************************************************************
Created by: Avery Johnson
Course/Assignment: CS4760/Project 4
Due Date: 20230404

Notes: See README or help documentation through option -h for further information.

Sources (aside from textbook or man pages):
- None
****************************************************************************************************/

//preprocessor directives
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <signal.h>
#include <sys/types.h>
#include <mqueue.h>
#include <ctime>
#include <queue>
#include <vector>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <cmath>
#include <math.h>

using namespace std;

//declare struct prior to prototypes since its used as param
struct PCB
{
	int occupied;
	int blocked;
	int pid;
	int startSec;
	int startNano;
};

//declare struct for ready/blocked queues
struct queueItem
{
	int pid;
	int timeWaitingSec;
	int timeWaitingNano;
	int timeBlockedSec;
	int timeBlockedNano;
	int unblockedSec;
	int unblockedNano;
};

//struct for sec/nano used in blocked/ready vector totals
struct timePassed
{
	int sec;
	int nano;
};

//typedef and macro for sysV mq
#define PERMS 0644
typedef struct msgbuffer
{
	long mtype;
	int intData;
} msgbuffer;

//global process tree to be used in signal functions, global ints for mq and file line counter
struct PCB processTable[18];
int msqid;
int fileLineCount;

//prototypes for funcs labeled below main (some funcs below for organizational purposes)
void printProcTable(struct PCB procTable[], int sec, int nano, FILE* logFile);
void removeProcess(struct PCB procTable[], int pid);
void addProcess(struct PCB procTable[], int pid, int sec, int nano);

//function to add wait/blocked time to each item in both blocked and ready queues
queue<queueItem> addTimes(queue<queueItem> myQ, char qFlag, int time)
{
	int size = myQ.size();
	for (int i = 0 ; i < size ; i ++)
	{
		queueItem temp;
		temp.pid = myQ.front().pid;
		temp.unblockedSec = myQ.front().unblockedSec;
		temp.unblockedNano = myQ.front().unblockedNano;
		//if marked as blocked queue, add times for blocking
		if (qFlag == 'b')
		{
			temp.timeBlockedNano = myQ.front().timeBlockedNano + time;
			temp.timeBlockedSec = myQ.front().timeBlockedSec;
			if (temp.timeBlockedNano >= 1000000000)
			{
				temp.timeBlockedNano -= 1000000000;
				temp.timeBlockedSec++;
			}
			temp.timeWaitingSec = myQ.front().timeWaitingSec;
			temp.timeWaitingNano = myQ.front().timeWaitingNano;
		}
		//otherwise add for ready wait times
		else
		{
			temp.timeWaitingNano = myQ.front().timeWaitingNano + time;
			temp.timeWaitingSec = myQ.front().timeWaitingSec;
			if (temp.timeWaitingNano >= 1000000000)
			{
				temp.timeWaitingNano -= 1000000000;
				temp.timeWaitingSec++;
			}
			temp.timeBlockedNano = myQ.front().timeBlockedNano;
			temp.timeBlockedSec = myQ.front().timeBlockedSec;
		}
		myQ.push(temp);
		myQ.pop();
	}
	return myQ;
}

//log function for OSS, logFlag is a char that states what kind of log it is, timeTaken
void OSSLog(int timeTakenNano, char logFlag, int sec, int nano, FILE* file, int pid)
{
	fileLineCount++;
	if (logFlag == 'g') //generating new process
	{
		printf("OSS: Generating process with PID %d and putting it in ready queue at time %d:%d, taking %d nanoseconds\n", pid, sec, nano, timeTakenNano);
		if (fileLineCount <= 10000)
			fprintf(file, "OSS: Generating process with PID %d and putting it in ready queue at time %d:%d, taking %d nanoseconds\n", pid, sec, nano, timeTakenNano);
	}
	else if (logFlag == 'd') //dispatching worker process
	{
		printf("OSS: Dispatching process with PID %d from ready queue at time %d:%d, taking %d nanoseconds\n", pid, sec, nano, timeTakenNano);
		if (fileLineCount <= 10000)
			fprintf(file, "OSS: Dispatching process with PID %d from ready queue at time %d:%d, taking %d nanoseconds\n", pid, sec, nano, timeTakenNano);
	}
	else if (logFlag == 's') //dispatcher dispatching itself
	{
		printf("OSS: No process in ready queue, OSS dispatching itself at time %d:%d for %d nanoseconds\n", sec, nano, timeTakenNano);
		if (fileLineCount <= 10000)
			fprintf(file, "OSS: No process in ready queue, OSS dispatching itself at time %d:%d for %d nanoseconds\n", sec, nano, timeTakenNano);
	}
	else if (logFlag == 'e') //receiving process
	{
		printf("OSS: receiving process with PID %d at time %d:%d, process ran for %d nanoseconds\n", pid, sec, nano, timeTakenNano);
		if (fileLineCount <= 10000)
			fprintf(file, "OSS: receiving process with PID %d at time %d:%d, process ran for %d nanoseconds\n", pid, sec, nano, timeTakenNano);
	}
	else if (logFlag == 'r') //putting process in ready queue
	{
		printf("OSS: moving process with PID %d to ready queue at time %d:%d, taking %d nanoseconds\n", pid, sec, nano, timeTakenNano);
		if (fileLineCount <= 10000)
			fprintf(file, "OSS: moving process with PID %d to ready queue at time %d:%d, taking %d nanoseconds\n", pid, sec, nano, timeTakenNano);
	}
	else if (logFlag == 't') //terminating process
	{
		printf("OSS: process with PID %d terminating at time %d:%d, taking %d nanoseconds\n", pid, sec, nano, timeTakenNano);
		if (fileLineCount <= 10000)
			fprintf(file, "OSS: process with PID %d terminating at time %d:%d, taking %d nanoseconds\n", pid, sec, nano, timeTakenNano);
	}
	else if (logFlag == 'b') //if a process is being blocked
	{
		printf("OSS: moving process with pid %d to blocked queue at time %d:%d, taking %d nanoseconds\n", pid, sec, nano, timeTakenNano);
		if (fileLineCount <= 10000)
			fprintf(file, "OSS: moving process with pid %d to blocked queue at time %d:%d, taking %d nanoseconds\n", pid, sec, nano, timeTakenNano);
	}
	else if (logFlag = 'u') //if process is being moved from blocked to ready queue
	{
		printf("OSS: moving process with pid %d from blocked to ready queue at time %d:%d, taking %d nanoseconds\n", pid, sec, nano, timeTakenNano);
		if (fileLineCount <= 10000)
			fprintf(file, "OSS: moving process with pid %d from blocked to ready queue at time %d:%d, taking %d nanoseconds\n", pid, sec, nano, timeTakenNano);
	}
}

//func to call worker
void callWorker()
{
        char* args [] = {"worker", 0};
        execv("./worker", args);
}

//func to increment time (just for organization)
void incrTime(int* sec, int* nano, int nanoInc)
{
	*nano += nanoInc;
	if (*nano >= 1000000000)
	{
		*nano -= 1000000000;
		(*sec)++;
	}
}

//func that states what to run once SIGINT is caught (ctrl + c)
void sigCatcher(int sig)
{
	//unlink shared mem
	shm_unlink("/secMemSeg");
	shm_unlink("/nanoMemSeg");

        //get rid of sysV msq
        if (msgctl(msqid, IPC_RMID, NULL) == -1)
        {
                printf("issue killing mq on parentside\n");
                exit(1);
        }

	//terminate entire group
	killpg(getpid(), SIGTERM);
}

//code that was given to us for limiting process interval
//this func states what actually happens
static void myhandler(int s) 
{
	//unlink shared mem
	shm_unlink("/secMemSeg");
	shm_unlink("/nanoMemSeg");


        //get rid of sysV msq
        if (msgctl(msqid, IPC_RMID, NULL) == -1)
        {
                printf("issue killing mq on parentside\n");
                exit(1);
        }

	//terminate entire group
	killpg(getpid(),SIGTERM);
}
//this func establishes the interrupt
static int setupinterrupt(void) 
{
	struct sigaction act;
	act.sa_handler = myhandler;
	act.sa_flags = 0;
	return (sigemptyset(&act.sa_mask) || sigaction(SIGPROF, &act, NULL));
}
//this func establishes the timer, started at max 2 seconds, changed to 60 per requirements
static int setupitimer(void) 
{
	struct itimerval value;
	value.it_interval.tv_sec = 60;
	value.it_interval.tv_usec = 0;
	value.it_value = value.it_interval;
	return (setitimer(ITIMER_PROF, &value, NULL));
}

//entry point
int main(int argc, char* argv[])
{
	//define init value for line count
	fileLineCount = 0;

	//calls for error handling in relation to setting the program interval
	if (setupinterrupt() == -1)
	{
		printf("Error setting up interrupt handler, terminating...\n");
		exit(1);
	}
	if (setupitimer() == -1)
	{
		printf("Error setting up interval timer, terminating...\n");
		exit(1);
	}
	//establish what happens if user tries ctrl + c
	signal(SIGINT, sigCatcher);

	//declare queues and vectors for blocked/ready
	queue<queueItem> readyQueue;
	queue<queueItem> blockedQueue;
	vector<timePassed> blockedTotals;
	vector<timePassed> readyTotals;

	//declare resources for mq and touch mq file
	msgbuffer sndBuffer, rcvBuffer;
	key_t key;
	system("touch msgq.txt");

	//ftok to get key for message queue
	if ((key = ftok("msgq.txt", 1)) == -1)
	{
		printf("ftok issue on parentside\n");
		exit(1);
	}	

	//set up msg queue
	if ((msqid = msgget(key, PERMS | IPC_CREAT)) == -1)
	{
		printf("issue with msgget in parent\n");
		exit(1);
	}

	//setup start message that will be sent to workers through queues
	string startString = "Start";
	char* startMessage = new char[startString.length() + 1];
	copy(startString.begin(), startString.end(), startMessage);

	//fill process tree with zeros
	for (int i; i < 18 ;i++)
	{
		processTable[i].occupied = 0;
		processTable[i].blocked = 0;
		processTable[i].pid = 0;
		processTable[i].startSec = 0;
		processTable[i].startNano = 0;
	}

        //declare opt, but define default values for opt args
        int opt;
	string logString = "defaultLog";

        //loop through command line entry until no opts/args remain
        while ((opt = getopt(argc, argv, "hf:")) != -1)
        {
                switch (opt)
                {
                        //incase of help option, show user the help message.
                        case 'h':
                                printf("\n!OSS Help! (Check README for further help information)\n\n");
                                printf("-Please use './oss' for envocation.\n-If you want to set the logFile that the output will be pushed to, an option will need to be used.\n");
                                printf("-An example command is 'oss -f myLog', the command outputs the 'oss' output to the terminal and to the logfile, 'myLog'.\n");
                                printf("\nFor clarity, the options and their meanings are listed below (option -f needs an additional argument):\n");
                                printf("-option '-h' envokes the help portion.\n");
				printf("-option '-f' and its following argument specify the logFile that 'oss' pushes its output to.\n");
                                exit(0);
                        //if proper opt other than -h is used, set the relative variable to the value that is input at the relative arg position
			case 'f':
				logString = optarg;
				printf("LogString is %s\n", logString.c_str());
				break;
                        default:
                                printf("ERROR: An incorrect option was entered, or an argument is missing after an option that expects an argument!\n");
                                exit(1);
                }
        }
	//make FILE* from logString
	FILE* logFile = fopen(logString.c_str(), "w");

	//create shared mem sec and nano vars
	//set up sec destination, throw error if issue occurs
	int secMemSeg = shm_open("/secMemSeg", O_RDWR|O_CREAT, 0777);
	if (secMemSeg < 0)
	{
		printf("issue creating sec mem seg\n");
		exit(1);
	}
	//configure size of sec mem seg
	ftruncate(secMemSeg, sizeof(int));
	//use mmap to map sec address, throw relative error if issue occurs
	void* secAddress = mmap(NULL, sizeof(int), PROT_READ|PROT_WRITE, MAP_SHARED, secMemSeg, 0);
	if (secAddress == MAP_FAILED)
	{
		printf("issue setting up sec address\n");
		exit(1);
	}
	//set that address as the value of a pointer to the sec int value
	int* sysSec = (int*)secAddress;
	*sysSec = 0;

	//set up nano mem seg, throw error if issue occurs
	int nanoMemSeg = shm_open("/nanoMemSeg", O_RDWR|O_CREAT, 0777);
	if (nanoMemSeg < 0)
	{
		printf("Issue creating nano mem seg\n");
		exit(1);
	}
	//configure size of nano mem seg
	ftruncate(nanoMemSeg, sizeof(int));
	//use mmap to map nano address, throw relative error if issue occurs
	void* nanoAddress = mmap(NULL, sizeof(int), PROT_READ|PROT_WRITE, MAP_SHARED, nanoMemSeg, 0);
	if (nanoAddress == MAP_FAILED)
	{
		printf("issue setting up nano address\n");
		exit(1);
	}
	//set shared address as value of pointer to the nano int value
	int* sysNano = (int*)nanoAddress;
	*sysNano = 0;

	//ints to keep track of how many children have been terminated, how many are currently running and have been launched
	int terminatedChildren = 0;
	int launchedChildren = 0;
	int maxWorkers = 100;
	bool maxTimeFlag = false;
	int lastPrint = 0;
	
	//seed the rng and create max time between sec and nano before starting primary loop
	srand(time(NULL));
	int mtbSec = 0;
	int mtbNano = 500000000;

	//create nextTimeFor and nextInterval vars for new process and set initial value
	//manually setting init values to 0 so we start out with 1 process able to run
	int niSec = rand() % (mtbSec + 1);
	int niNano = 0;
	if (niSec == mtbSec)
		niNano = rand() % (mtbNano + 1);
	else
		//should be (999999999 + 1) if mtbSec is above 0
		niNano = rand() % (500000000 + 1);
	int ntfSec = 0;//niSec;
	int ntfNano = 0;//niNano;

	//set start time for 
	time_t loopStart = time(0);

	//max blocked and interval blocked queue vars
	int maxBlockedSec = 5;
	int maxBlockedNano = 100000000;
	int intBlockedSec = 0;
	int intBlockedNano = 0;

	//cpu percentage variables and empty ready queue vars
	int cpuInUseSec = 0;
	int cpuInUseNano = 0;
	int emptyReadySec = 0;
	int emptyReadyNano = 0;

	//max time a proc can run for
	int quantum = 10000000;
	
	//primary system loop, stops looping when all children have terminated
	while (terminatedChildren < maxWorkers)
	{
		//if 3 realsecs have passed, set max workers to the current amount of launched workers + 1 to stop launching new workers
		if (!maxTimeFlag && time(0) - loopStart >= 3)
		{
			maxTimeFlag = true;
			maxWorkers = launchedChildren;
		}

		//print procTable for testing purposes
		if (*sysSec > lastPrint)
		{
			lastPrint = *sysSec;
			printProcTable(processTable, *sysSec, *sysNano, logFile);
		}

		//if enough time's passed, there's space to launch a child, and children still need launched, launch one
		if ((*sysSec > ntfSec || *sysSec == ntfSec && *sysNano >= ntfNano) && launchedChildren < maxWorkers && processTable[17].occupied == 0)
		{
			launchedChildren++;
			//roll new interval/next times
			niSec = rand() % (mtbSec + 1);
			if (niSec == mtbSec)
				niNano = rand() % (mtbNano + 1);
			else
				niNano = rand() % (500000000 + 1);
			ntfSec = (*sysSec) + niSec;
			ntfNano = (*sysNano) + niNano;
			if (ntfNano >= 1000000000)
			{
				ntfSec++;
				ntfNano -= 1000000000;
			}

			//increase the launchedChildren count and fork/exec a child
			int forkPid = fork();
			//create worker if child
			if (forkPid == 0)
				callWorker();
			else if (forkPid == -1)
			{
				printf("issue forking\n");
				exit(1);
			}
			//otherwise, add to procTable, vector and queue(through temp queueItem proxy), print, incr time, etc.
			else
			{
				//add to table
				addProcess(processTable, forkPid, *sysSec, *sysNano);

				//increase time in queues for stat tracking before added to ready queue since new proc isn't waiting but being gen'd
				if (blockedQueue.size() > 0)
					blockedQueue = addTimes(blockedQueue, 'b', 100000);
				if (readyQueue.size() > 0)
					readyQueue = addTimes(readyQueue, 'r', 100000);

				//add to queue
				queueItem tempItem;
				tempItem.pid = forkPid;
				tempItem.timeWaitingSec = 0;
				tempItem.timeWaitingNano = 0;
				tempItem.timeBlockedSec = 0;
				tempItem.timeBlockedNano = 0;
				tempItem.unblockedSec = 0;
				tempItem.unblockedSec = 0;
				readyQueue.push(tempItem);

				//increase time and log
				incrTime(sysSec, sysNano, 100000);
				OSSLog(100000, 'g', *sysSec, *sysNano, logFile, forkPid);
			}
		}

		//do this if the blocked queue's not empty
		if (blockedQueue.size() > 0)
		{
			//if the current pseudo system time is greater than or equal to the time needed for unblock of the front of blocked queue
			if (*sysSec > blockedQueue.front().unblockedSec || *sysSec == blockedQueue.front().unblockedSec && *sysNano >= blockedQueue.front().unblockedNano)
			{
				//incr time and log the moving of blocked to ready for process
				incrTime(sysSec, sysNano, 500000);
				OSSLog(500000, 'u', *sysSec, *sysNano, logFile, blockedQueue.front().pid);

				//change the 'blocked' portion of the relative PCB in the process table back to 0
				for (int i = 0 ; i < 18 ; i++)
				{
					if (processTable[i].pid == blockedQueue.front().pid)
					{
						processTable[i].blocked = 0;
						break;
					}
				}

				//increase stat times for waiting procs
				blockedQueue = addTimes(blockedQueue, 'b', 500000);
				if (readyQueue.size() > 0)
					readyQueue = addTimes(readyQueue, 'r', 500000);

				//actually move the blocked process back to the ready queue
				readyQueue.push(blockedQueue.front());
				blockedQueue.pop();
			}
		}

		//proceed if readyQueue has entries
		if (readyQueue.size() > 0)
		{
			//increment time and log that we're dispatching a ready process
			incrTime(sysSec, sysNano, 100000);
			OSSLog(100000, 'd', *sysSec, *sysNano, logFile, readyQueue.front().pid);

			//increase stat time for waiting procs
			if (blockedQueue.size() > 0)
				blockedQueue = addTimes(blockedQueue, 'b', 100000);
			readyQueue = addTimes(readyQueue, 'r', 100000);

			//send start message to front worker and wait for a response back
			sndBuffer.mtype = readyQueue.front().pid;
			sndBuffer.intData = quantum;
			if (msgsnd(msqid, &sndBuffer, sizeof(msgbuffer) - sizeof(long), 0) == -1)
			{
				printf("sending issue from parentside\n");
				exit(1);
			}

			//receive message and push repsonse to log and increment time accordingly
			if (msgrcv(msqid, &rcvBuffer, sizeof(msgbuffer), getpid(), 0) == -1)
			{
				printf("issue receiving from parentside\n");
				exit(1);
			}
			incrTime(sysSec, sysNano, abs(rcvBuffer.intData));
			OSSLog(abs(rcvBuffer.intData), 'e', *sysSec, *sysNano, logFile, readyQueue.front().pid);

			//increasing stat times for waiting procs
			if (blockedQueue.size() > 0)
				blockedQueue = addTimes(blockedQueue, 'b', 100000);
			readyQueue = addTimes(readyQueue, 'r', 100000);
			
			//increase CPU usage time by return value
			cpuInUseNano += abs(rcvBuffer.intData);
			if (cpuInUseNano >= 1000000000)
			{
				cpuInUseNano -= 1000000000;
				cpuInUseSec++;
			}

			//if response is negative, process terminated, so remove from queue and process table
			if (rcvBuffer.intData < 0)
			{
				//add process info to relative stat vectors
				timePassed tempTime;
				tempTime.sec = readyQueue.front().timeWaitingSec;
				tempTime.nano = readyQueue.front().timeWaitingNano;
				readyTotals.push_back(tempTime);
				if(readyQueue.front().timeBlockedSec > 0 || readyQueue.front().timeBlockedNano > 0)
				{
					tempTime.sec = readyQueue.front().timeBlockedSec;
					tempTime.nano = readyQueue.front().timeBlockedNano;
					blockedTotals.push_back(tempTime);
				}

				incrTime(sysSec, sysNano, 100000);
				OSSLog(100000, 't', *sysSec, *sysNano, logFile, readyQueue.front().pid);
				removeProcess(processTable, readyQueue.front().pid);
				readyQueue.pop();
				
				//increase time stats for procs
				if (readyQueue.size() > 0)
					readyQueue = addTimes(readyQueue, 'r', 100000);
				if (blockedQueue.size() > 0)
					blockedQueue = addTimes(blockedQueue, 'b', 100000);

				terminatedChildren++;
				wait(NULL);
			}
			//else if it's exactly the quantum of 10000000, it just gets put back in the ready queue
			else if (rcvBuffer.intData == quantum)
			{
				incrTime(sysSec, sysNano, 100000);
				OSSLog(100000, 'r', *sysSec, *sysNano, logFile, readyQueue.front().pid);
				readyQueue.push(readyQueue.front());
				readyQueue.pop();

				//increase wait times for proc stats
				if (blockedQueue.size() > 0)
					blockedQueue = addTimes(blockedQueue, 'b', 100000);
				readyQueue = addTimes(readyQueue, 'r', 100000);
			}
			//else it's blocked
			else
			{
				//get random time for process to be unblocked
				intBlockedSec = rand() % (maxBlockedSec + 1);
				if (intBlockedSec == maxBlockedSec)
					intBlockedNano = rand() % (maxBlockedNano + 1);
				else
					intBlockedNano = rand() % (999999999 + 1);

				//attach this time to the relative process
				readyQueue.front().unblockedSec = (*sysSec) + intBlockedSec;
				readyQueue.front().unblockedNano = (*sysNano) + intBlockedNano;
				if (readyQueue.front().unblockedNano >= 1000000000)
				{
					readyQueue.front().unblockedNano -= 1000000000;
					readyQueue.front().unblockedSec++;
				}

				//update the processTable to show that the relative pid is blocked
				for (int i = 0 ; i < 18 ; i++)
				{
					if (processTable[i].pid == readyQueue.front().pid)
					{
						processTable[i].blocked = 1;
						break;
					}
				}
				
				//increase time stats for procs in queues
				if (blockedQueue.size() > 0)
					blockedQueue = addTimes(blockedQueue, 'b', 100000);
				readyQueue = addTimes(readyQueue, 'r', 100000);

				//increment the time, log it and send the process to the blocked queue, removing it from ready
				incrTime(sysSec, sysNano, 100000);
				OSSLog(100000, 'b', *sysSec, *sysNano, logFile, readyQueue.front().pid);
				blockedQueue.push(readyQueue.front());
				readyQueue.pop();
			}
		}
		//else, assume OSS will be dispatching itself in order to progress time and allow for jobs to be created
		else
		{
			incrTime(sysSec, sysNano, 10000000);
			OSSLog(10000000, 's', *sysSec, *sysNano, logFile, 0);

			//increase stat counter for amount of time spent with an empty ready queue
			emptyReadyNano += 10000000;
			if (emptyReadyNano >= 1000000000)
			{
				emptyReadyNano -= 1000000000;
				emptyReadySec++;
			}
		}
	}
	//final table print to show it's empty (fileprint commented out but left in if you want it)
	printf("\nTABLE AT TERMINATION:\n");
	//fprintf(logFile, "TABLE AT TERMINATION:\n");
	printProcTable(processTable, *sysSec, *sysNano, logFile);
	
	//get rid of sysV msq
	if (msgctl(msqid, IPC_RMID, NULL) == -1)
	{
		printf("issue killing mq on parentside\n");
		exit(1);
	}

	//calculate the total blocked/readywait Time
	long totalBlockedSec = 0;
	long totalBlockedNano = 0;
	long totalReadySec = 0;
	long totalReadyNano = 0;
	for (int i = 0; i < blockedTotals.size() ; i++)
	{
		totalBlockedSec += blockedTotals[i].sec;
		totalBlockedNano += blockedTotals[i].nano;
		if (totalBlockedNano >= 1000000000)
		{
			totalBlockedNano -= 1000000000;
			totalBlockedSec++;
		}
	}
	for (int i = 0; i < readyTotals.size() ; i++)
	{
		totalReadySec += readyTotals[i].sec;
		totalReadyNano += readyTotals[i].nano;
		if (totalReadyNano >= 1000000000)
		{
			totalReadyNano -= 1000000000;
			totalReadySec++;
		}
	}
	long totalWaitSec = totalBlockedSec + totalReadySec;
	long totalWaitNano = totalBlockedNano + totalReadyNano;
	if (totalWaitNano >= 1000000000)
	{
		totalWaitNano -= 1000000000;
		totalWaitSec++;
	}

	//calculate average blocked/wait sec and nanos
	double averageBlockedSec = (double)totalBlockedSec / blockedTotals.size();
	double averageBlockedNano = (double)totalBlockedNano / blockedTotals.size();
	double averageWaitSec = (double)totalWaitSec / terminatedChildren;
	double averageWaitNano = (double)totalWaitNano / terminatedChildren;

	//calculate the average CPU utilization
	double totalUseCPU = cpuInUseSec + (cpuInUseNano * 0.000000001);
	double totalSysTime = (*sysSec) + ((*sysNano) * 0.000000001);
	double avgUtilCPU = totalUseCPU / totalSysTime;
	float fpAvgUtil = roundf(avgUtilCPU * 10000) / 100;

	//print final stats
	printf("\nSystem Stats:\nAverage blocked time - %fs:%fns\n", averageBlockedSec, averageBlockedNano);
	printf("Average wait time - %fs:%fns\n", averageWaitSec, averageWaitNano);
	printf("Total time spent with an empty ready queue - %ds:%dns\n", emptyReadySec, emptyReadyNano);
	printf("Average CPU Utilization (percentage of time CPU was occupied by a worker) - %f%\n", fpAvgUtil);

	//unlink shared memory
	shm_unlink("/secMemSeg");
	shm_unlink("/nanoMemSeg");

	//close logfile
	fclose(logFile);

        return 0;
}

//function to loop through process table and print it out in a more readable manner
//fileprints are just commented out incase you want them in your file
void printProcTable(struct PCB procTable[], int sec, int nano, FILE* logFile)
{
	printf("OSS PID:%d SysClock: %d SysClockNano: %d\n", getpid(), sec, nano);
	//fprintf(logFile, "OSS PID:%d SysClock: %d SysClockNano: %d\n", getpid(), sec, nano);
	printf("Process Table:\n");
	//fprintf(logFile, "Process Table:\n");
	printf("Entry\tOccupied Blocked PID\tStartS\tStartN\n");
	//fprintf(logFile, "Entry\tOccupied PID\tStartS\tStartN\n");
	for (int i = 0 ; i < 18 ; i++)
	{
		printf("%d\t%d\t %d\t %d\t%d\t%d\n", i, procTable[i].occupied, procTable[i].blocked, procTable[i].pid, procTable[i].startSec, procTable[i].startNano);
		//fprintf(logFile, "%d\t%d\t %d\t%d\t%d\n", i, procTable[i].occupied, procTable[i].pid, procTable[i].startSec, procTable[i].startNano);
	}
}

//function that loops through the process table to find a relative pid and remove it's line while moving the rest back together
void removeProcess(struct PCB procTable[], int pid)
{
	bool found = false;
	for (int i = 0 ; i < 18 ; i++)
	{
		if (procTable[i].pid == pid)
		{
			//if assessing the final position of the array, zero it out
			if (i == 17)
			{
				procTable[i].occupied = 0;
				procTable[i].blocked = 0;
				procTable[i].pid = 0;
				procTable[i].startSec = 0;
				procTable[i].startNano = 0;
				return;
			}
			//otherwise, set the positional value equal to the next indexed value, moves everything to the "left" by one.
			else
			{
				for (int j = i ; j < 17 ; j++)
				{
					procTable[j].occupied = procTable[j + 1].occupied;
					procTable[j].blocked = procTable[j + 1].blocked;
					procTable[j].pid = procTable[j + 1].pid;
					procTable[j].startSec = procTable[j + 1].startSec;
					procTable[j].startNano = procTable[j + 1].startNano;
				}
				procTable[17].occupied = 0;
				procTable[17].blocked = 0;
				procTable[17].pid = 0;
				procTable[17].startSec = 0;
				procTable[17].startNano = 0;
				return;
			}
		}
	}
}

//function to go to lowest empty position in the process table and enter the relative child information
void addProcess(struct PCB procTable[], int pid, int sec, int nano)
{
	for (int i = 0 ; i < 18 ; i++)
	{
		if (procTable[i].occupied == 0)
		{
			procTable[i].occupied = 1;
			procTable[i].pid = pid;
			procTable[i].startSec = sec;
			procTable[i].startNano = nano;
			return;
		}
	}
}
