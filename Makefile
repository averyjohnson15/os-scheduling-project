all: oss worker

oss: oss.cpp
	g++ -g -std=c++11 oss.cpp -o oss -lrt

worker: worker.cpp
	g++ -g -std=c++11 worker.cpp -o worker -lrt

clean:
	rm oss worker
